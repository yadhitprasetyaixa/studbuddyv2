from django.urls import path

from . import views

app_name = 'saran'

urlpatterns = [
    path('', views.landingsaran, name='saranland'),
]