from django.shortcuts import render, redirect
from .models import Saran
from django.utils import timezone
from django.http import JsonResponse, HttpResponse

# Create your views here.
def landingsaran(request):
    saran = Saran.objects.all().order_by('-time')
    ukuran = Saran.objects.all().count()

    if request.method == "POST":
        nama = request.POST.get("user")
        saran = request.POST.get("saran")
        
        p = Saran.objects.create(nama = nama, saran = saran)
        p.save()
        return redirect('/saran/')

    context = {
        'size' : ukuran,
    }
    return render(request, 'saran/landingsaran.html',context)

def ajax_update(request):
    
    data = list(Saran.objects.values().order_by('-time'))  # wrap in list(), because QuerySet is not JSON serializable
    return JsonResponse(data, safe=False)

    