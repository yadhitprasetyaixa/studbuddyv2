from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Saran(models.Model):
    nama = models.TextField(max_length=300, blank=False, default="test")
    saran = models.TextField(max_length=300, blank=False)
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.saran