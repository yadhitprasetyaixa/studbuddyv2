from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import landingsaran
from django.utils import timezone
from django.http import HttpRequest
from django.conf import settings
from importlib import import_module
from django.contrib.auth.models import User
from .models import Saran
# Create your tests here.

class SaranTest (TestCase):
    def test_url_exist (self):
        response = self.client.get('/saran/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/saran/')
        self.assertEqual(found.func, landingsaran)

    def test_landing_page_uses_template(self):
        response = self.client.get('/saran/')
        self.assertTemplateUsed(response, 'saran/landingsaran.html')
    
    def test_unloged_user_views(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = landingsaran(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Silahkan login untuk melanjutkan", html_response)

    def test_model_can_create_new_saran(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        Saran.objects.create(
            nama = 'bambang',
            saran = 'Minggu ini santuy parah',
            time = timezone.now()
        )

        count_all_saran = Saran.objects.all().count()
        self.assertEqual(count_all_saran, 1)

    def test_model_str_function(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        obj = Saran.objects.create(
            nama = 'bambang',
            saran = 'Minggu ini santuy parah',
            time = timezone.now()
        )

        self.assertTrue(isinstance(obj, Saran))
        self.assertEqual(obj.__str__(), obj.saran)
    
    def test_form_success(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        c.login(username='bambang', password='12345')
        c.post('/saran/', {
            'saran':'testtbro',
            'user':'bambang'
            })
        count_all_saran = Saran.objects.all().count()
        self.assertEqual(count_all_saran, 1)
