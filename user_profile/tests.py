from django.contrib import auth
from django.contrib.auth.models import User
from django.http import response
from django.test import TestCase , Client
from django.urls import resolve

# Create your tests here.

class UnitTestForProfile(TestCase):
    def test_response_page(self):
        response = Client().get('/user_profile/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_for_begin(self):
        response = Client().get('/user_profile/')
        self.assertTemplateUsed(response, 'index.html')