from django.db import models
from django.utils import timezone

#for post blog feature (gajadi keanya soalnya error mulu Sad)
class Post(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.FileField(blank=True)

    def __str__(self):
        return self.title

#for Todo feature
class Category(models.Model): # The Category table name that inherits models.Model
    name = models.CharField(max_length=100) #Like a varchar
    class Meta:
        verbose_name = ("Category")
        verbose_name_plural = ("Categories")
    def __str__(self):
        return self.name #name to be shown when called
class TodoList(models.Model): #Todolist able name that inherits models.Model
    title = models.CharField(max_length=250) # a varchar
    content = models.TextField(blank=True) # a text field 
    created = models.DateField(default=timezone.now().strftime("%Y-%m-%d")) # a date
    due_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d")) # a date
    category = models.ForeignKey(Category, default="general",  on_delete=models.CASCADE) # a foreignkey
    class Meta:
        ordering = ["-created"] #ordering by the created field
    def __str__(self):
        return self.title #name to be shown when called



# Create your models here.
#class user_profile(models.Model):
#	user = models.OneToOneField(User, null = True, on_delete = models.CASCADE)
#	name = models.CharField(max_length = 200, null = True)
#	institusi = models.CharField(max_length = 200, null = True)
#	jurusan = models.CharField(max_length = 200, null = True)
#	profile_pic = models.ImageField(null = True, blank = True)

#	def __str__(self):
#		return self.name

# Create your models here. #DataFlair 

