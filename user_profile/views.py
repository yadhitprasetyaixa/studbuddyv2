from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.contrib import messages 
from django.urls import reverse

#for Todo Feature
from .models import TodoList, Category

#For blog feature
from .models import Post

#mau nyoba biar user bisa masukin foto trus ditunjukin tapi gagal
def user_profile(request):
    response = {}
    return render(request, "index.html", response)

#blog feature

def blog_view(request):
    posts = Post.objects.all().order_by('-id')
    return render(request, 'blog.html', {'posts':posts})

def create_post_view(request):
    if request.POST.get('action') == 'create-post':
        title = request.POST.get('title')
        description = request.POST.get('description')
        image = request.FILES.get('image') # request.FILES used for to get files
 
        Post.objects.create(
            title=title,
            description=description,
            image=image
        )
 
    return render(request, 'create-post.html')

#Todo feature
def index(request): #the index view
    todos = TodoList.objects.all() #quering all todos with the object manager
    categories = Category.objects.all() #getting all categories with object manager
    if request.method == "POST": #checking if the request method is a POST
        if "taskAdd" in request.POST: #checking if there is a request to add a todo
            title = request.POST["description"] #title
            date = str(request.POST["date"]) #date
            category = request.POST["category_select"] #category
            content = title + " -- " + date + " " + category #content
            Todo = TodoList(title=title, content=content, due_date=date, category=Category.objects.get(name=category))
            Todo.save() #saving the todo 
            return redirect("/user_profile") #reloading the page
        if "taskDelete" in request.POST: #checking if there is a request to delete a todo
            checkedlist = request.POST["checkedbox"] #checked todos to be deleted
            if checkedlist:
                for todo_id in checkedlist:
                    todo = TodoList.objects.get(id=int(todo_id)) #getting todo id
                    todo.delete() #deleting todo
            else:
                return HttpResponse('')
    return render(request, "index.html", {"todos": todos, "categories":categories})