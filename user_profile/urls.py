from django.contrib import admin 
from django.urls import path 
from . import views 
from user_profile.views import user_profile, blog_view,create_post_view, index
from django.conf import settings 
from django.conf.urls.static import static 

app_name = 'user_profile'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="profile"),
    path('blog/', blog_view, name='blog'),
    path('create-post/',create_post_view, name='create-post')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)