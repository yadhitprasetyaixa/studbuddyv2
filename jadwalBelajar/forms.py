from django import forms

class jadwalForm(forms.Form):
    error_message = {
        'required' : 'This field is required'
    }
    nama = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan Nama Anda', 'style':'border-radius: 20px; text-align: center;' "background-color: #9b9b9b55"}), label='Nama', max_length=50, required=True)
    matkul = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'Masukkan Nama Mata Kuliah', 'style':'border-radius: 20px; text-align: center;' "background-color: #9b9b9b55"}), label='Mata Kuliah', max_length=50, required=True)
    waktu = forms.TimeField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'Masukkan Waktu (JJ:MM)', 'style':'border-radius: 20px; text-align: center;' "background-color: #9b9b9b55"}), label="Waktu", required=True)
    link = forms.URLField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'Masukkan Link Ruang Kelas', 'style':'border-radius: 20px; text-align: center;' "background-color: #9b9b9b55"}), label='Link Ruang Kelas', required =True)
