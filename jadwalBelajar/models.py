from django.db import models

# Create your models here.
class modelJadwal(models.Model):
    nama = models.CharField(max_length=50)
    matkul = models.CharField(max_length=50)
    waktu = models.TimeField()
    link = models.URLField()

    def __str__(self):
        return self.nama
