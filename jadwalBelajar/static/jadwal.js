$(document).ready(function(){
    $('#bton').click(function(){
        var nama = $("#nama").val();
        var matkul = $("#matkul").val();
        var waktu = $("#waktu").val();
        var link = $("#link").val();
        if((nama.length != 0) && (link.length != 0) && (matkul.length != 0) && (waktu.length != 0)){
            return true;
        }
        else{
            return true;
        }
    })

    $("#nama").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#nama").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#matkul").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#matkul").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#waktu").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#waktu").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#link").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#link").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })

    $("form").on("submit", function(e) {
        var isiForm  = JSON.parse(JSON.stringify(jQuery(this).serializeArray()))
        $.ajax({
            type: "POST",
            url: "/jadwal/post",
            data: isiForm,
            success: function(response){
                var result = '';
                for(var i = 0; i < response.length; i++){
                    var nama = response[i].fields.nama;
                    var matkul = response[i].fields.matkul;
                    var waktu = response[i].fields.waktu;
                    var link = response[i].fields.link;
                    result += '<tr>' + '<td>' + i+1 + '</td>' + 
                    '<td>' + '<p style="text-align: center;">' + nama + '</p>' + '</td>' +
                    '<td>' + '<p style="text-align: center;">' + matkul + '</p>' + '</td>' +
                    '<td>' + '<p style="text-align: center;">' + waktu + '</p>' + '</td>' +
                    '<td>' + '<a href="{{ i.link }}" target="_blank">' + '<p style="text-align: center;">' + 'Click to open' + '</p>' + '</a>' + '</td>'
                    '<td>' + '<a href="/jadwalBelajar/delete/{{ i.id }}">' + '<button type="submit" class="btn"style="border-radius: 15px; color: #ffffff; background-color: #e41818;"id="bton">' + 'X' + '</button>' + '</a>' + '</td>' + '</tr>'
                }
                $('#resultnya').append(result);
            }
        })
        e.preventDefault();
    })
})
