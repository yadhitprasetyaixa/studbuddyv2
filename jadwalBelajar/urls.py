from django.urls import path
from . import views

app_name = 'jadwalBelajar'

urlpatterns = [
    path('', views.jadwalBelajar, name='jadwalBelajar'),
    path('post', views.post_jadwal, name='post_jadwal'),
    path('data', views.dataJadwal),
    path('', views.jadwalBelajar, name = 'jadwal'),
    path('delete/<int:id>', views.delete_jadwal, name='delete_jadwal'),
]
