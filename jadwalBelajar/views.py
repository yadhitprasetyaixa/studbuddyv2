from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import modelJadwal
from .forms import jadwalForm

# Create your views here.

def post_jadwal(request):
    if request.method == 'POST':
        response_data = {}
        form = jadwalForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['nama'] = request.POST['nama']
            response_data['matkul'] = request.POST['matkul']
            response_data['waktu'] = request.POST['waktu']
            response_data['link'] = request.POST['link']

            data_jadwal = modelJadwal(nama = response_data['nama'],
                                        matkul = response_data['matkul'],
                                        waktu = response_data['waktu'],
                                        link = response_data['link'],    
                                        )
            data_jadwal.save()
            return redirect('/jadwalBelajar/#jadwalTerdaftar')
        else:
            return redirect('/jadwalBelajar')
    else:
        return redirect('/jadwalBelajar')

def jadwalBelajar(request):
    form_tambah = jadwalForm()
    data = modelJadwal.objects.all()
    response ={
        'form_tambah' : form_tambah,
        'data' : data,

    }
    return render(request, 'jadwalbelajar.html', response)

def delete_jadwal(request, id):
    data = modelJadwal.objects.filter(id = id)
    data.delete()
    return redirect('/jadwalBelajar')

def dataJadwal(request):
    data_jadwal = modelJadwal.objects.all()
    jadwal_json = serializers.serialize('json', data_jadwal)
    isiData = json.loads(jadwal_json)
    return JsonResponse(isiData, safe=False)

