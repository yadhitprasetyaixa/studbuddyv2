from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http.response import JsonResponse
from .models import Song, Playlist
from .forms import Song_Form, Playlist_Form
from django.contrib.auth.decorators import login_required
import requests, json


# Create your views here.
def songlist(request):
    response = {'songlist' : Song.objects.all()}
    return render(request, 'playlist.html', response)

def addsong(request):
    response = {'s_form' : Song_Form}
    return render(request, 'addsong.html', response)

def savesong(request):
    song = Song.objects.all()
    form = Song_Form(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
    return HttpResponseRedirect('/songlib')
        
def delsong(request, kp):
    sg = Song.objects.get(id = kp)
    sg.delete()
    return HttpResponseRedirect('/songlib/')

def filterbycont(request, name):
    response = {'songs' : Song.objects.filter(artis__iexact=name),
                'cont' : name}
    return render(request, 'filterbycont.html', response)

def filterbygenre(request, genre):
    response = {'songs' : Song.objects.filter(genre=genre),
                'gen' : genre}
    return render(request, 'filterbygenre.html', response)

@login_required(login_url='/login_register/login/')
def view_playlist(request):
        response = {'mylist' : Playlist.objects.all()}
        return render(request, 'viewplaylist.html', response)
    
@login_required(login_url='/login_register/login/')
def create_playlist(request):
        response = {'songs' : Song.objects.all(), 'p_form' : Playlist_Form}
        return render(request, 'makeplaylist.html', response)
    
@login_required(login_url='/login_register/login/')
def save_playlist(request):
        plist = Playlist.objects.all()
        form = Playlist_Form(request.POST or None)
        if request.method == "POST":
            if form.is_valid():
                p = Playlist.objects.create(
                    nama= form.cleaned_data.get('nama')
                )
                p.contents.set(form.cleaned_data.get('contents'))
                p.save()
        return HttpResponseRedirect('/songlib/my')
    
@login_required(login_url='/login_register/login/')
def delete_playlist(request, kp):
    pl = Playlist.objects.get(id=kp)
    pl.delete()
    return HttpResponseRedirect('/songlib/my/')

@login_required(login_url='/login_register/login/')
def view_certain_playlist(request, nm):
        chosen = Playlist.objects.filter(nama=nm)
        cons = chosen.values_list('contents', flat=True)
        ch_list = []
        titles = []
        links = []
        for c in cons:
            qry = Song.objects.get(id=c)
            ch_list.append(qry)
        response = {'songpl': ch_list, 'plname':nm }
        return render(request, "inplaylist.html", response)
    
@login_required(login_url='/login_register/login/')
def del_playlist(request, str):
        plist = Playlist.objects.get(nama=str)
        plist.delete()
        return HttpResponseRedirect('/songlib/my/')
    