from django.urls import path
from django.conf.urls import url
from .views import songlist, addsong, savesong, delsong, filterbycont, filterbygenre, view_playlist, create_playlist
from .views import save_playlist, view_certain_playlist, delete_playlist

app_name = 'playlist'


urlpatterns = [
    path('', songlist, name="playlist"),
    path('addsong/', addsong),
    path('addsong/save', savesong, name="savesong"),
    path('delete/<int:kp>/', delsong, name="delsong"),
    path('filter/artis/<str:name>', filterbycont, name="filterbycont"),
    path('filter/genre/<str:genre>', filterbygenre, name="filterbygenre"),
    path('my/', view_playlist),
    path('addplaylist/', create_playlist),
    path('addplaylist/save',save_playlist),
    path('my/<str:nm>', view_certain_playlist, name="view_certain_playlist"),
    path('my/delete/<int:kp>', delete_playlist, name="delplaylist")

]
