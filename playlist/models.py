from django.db import models

# Create your models here.
class Song(models.Model):
    artis = models.CharField(max_length=50)
    judul = models.CharField(max_length=100)
    genre = models.CharField(max_length=50)
    link = models.CharField(max_length=300)

    def __str__(self):
        return self.artis + " - " + self.judul

class Playlist(models.Model):
    nama = models.CharField(max_length=30)
    contents = models.ManyToManyField(Song)
        
    def __str__(self):
        return self.nama