from django.forms import CharField
from django.forms.models import ModelForm
from django.forms.widgets import TextInput, CheckboxSelectMultiple
from .models import Song, Playlist

class Song_Form(ModelForm):
    class Meta:
        model = Song
        fields = ['artis', 'judul', 'genre', 'link']
    error_messages = {
        'required' : 'This field must be filled'
    }
    link = CharField(max_length=300, widget=TextInput(attrs={'placeholder':'ex: http://webname.com'}))


class Playlist_Form(ModelForm):
       
    class Meta:
        model = Playlist
        fields = ['nama', 'contents']
    error_messages = {
        'required' : 'This field cannot be empty'
    }
    nama = CharField(max_length=30, required=True)

    def __init__(self, *args, **kwargs):
        super(Playlist_Form, self).__init__(*args, **kwargs)

        self.fields['contents'].widget = CheckboxSelectMultiple()
        self.fields['contents'].queryset = Song.objects.all()