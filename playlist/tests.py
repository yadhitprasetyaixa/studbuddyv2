from django.test import TestCase, Client
from django.urls import resolve
from .models import Song, Playlist
from .forms import Song_Form
from django.forms.models import model_to_dict

# Create your tests here.
class TestActivity(TestCase):
        def test_url_playlist(self):
            response = Client().get('/songlib/')
            self.assertEquals(response.status_code, 200)

        def test_template_playlist(self):
            response = Client().get('/songlib/')
            self.assertTemplateUsed(response, 'playlist.html')
        
        def test_url_addsong(self):
            response = Client().get('/songlib/addsong/')
            self.assertEquals(response.status_code, 200)
        
        def test_template_addsong(self):
            response = Client().get('/songlib/addsong/')
            self.assertTemplateUsed(response, 'addsong.html')
        
        def test_create_song(self):
            Client().post('/songlib/addsong/save', data={'artis':'gw', 'judul':'apaya', 'genre':'pop', 'link':'https://google.com/'})
            count = Song.objects.filter(artis='gw').count()
            self.assertEqual(count, 1)

        def test_url_filterbycont(self):
            song = Song.objects.create(artis='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/songlib/filter/artis/gw')
            self.assertEquals(response.status_code, 200)

        def test_template_filterbycont(self):
            song = Song.objects.create(artis='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/songlib/filter/artis/gw')
            self.assertTemplateUsed(response, 'filterbycont.html')
        
        def test_url_filterbygenre(self):
            song = Song.objects.create(artis='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/songlib/filter/genre/rock')
            self.assertEquals(response.status_code, 200)

        def test_template_filterbygenre(self):
            song = Song.objects.create(artis='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/songlib/filter/genre/rock')
            self.assertTemplateUsed(response, 'filterbygenre.html')

        def test_delete_song(self):
            new_song = Song.objects.create(artis='gw', judul='apaya', genre='pop', link='https://google.com/')
            new_song.delete()
            count = Song.objects.all().count()
            self.assertEqual(count, 0)
        
        def test_add_playlist(self):
            Client().login(username='nurh', password='yeyeyeyet')
            song = Song.objects.create(artis='w', judul='call', genre='yuh', link='https://calendar.google.com/')
            self.assertEqual(str(song), 'w - call')
            np = Playlist.objects.create(nama='nyoba')
            np.contents.add(song)
            datas = model_to_dict(np)
            Client().post('/songlib/addplaylist/save', data=datas)
            count = Playlist.objects.filter(nama='nyoba').count()
            self.assertEqual(count, 1)
        
        def test_delete_playlist(self):
            new_play = Playlist.objects.create(nama='semauw')
            new_play.contents.create(artis='w', judul='hehe', genre='yuh', link='https://meet.google.com/')
            new_play.delete()
            count = Playlist.objects.all().count()
            self.assertEqual(count, 0)
        
        def test_url_playlist(self):
            response = Client().get('/songlib/my')
            self.assertEquals(response.status_code, 301)

        def test_url_playlist_loggedin(self):
            Client().login(username='nurh', password='yeyeyeyet')
            rep = Client().get('/songlib/my', follow=True)
            self.assertEquals(rep.status_code, 200)