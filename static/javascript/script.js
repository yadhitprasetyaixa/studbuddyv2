function executeQuery() {
    var url_data = "/data"
    $.ajax({
        url: url_data,
        success: function(data) {
            // Perform operation on return value
            var obj_hasil = $('.pencarian');
            obj_hasil.empty()

            for (i = 0; i < data.length; i++) {
                var nama = data[i].nama
                var saran = data[i].saran
                var waktu = data[i].time
                var date = new Date(waktu).toLocaleString()

                obj_hasil.append('<div class = "item"><div class = "item-head"><h3>' + nama + '</h3><p>' + date + '</p></div><div class = "item-isi"><p>' + saran + '</p></div></div>');
            }

        }
    });
    setTimeout(executeQuery, 5000);
}

$(document).ready(function() {

    // jQuery methods go here...
    setTimeout(executeQuery, 0);

    $("#uwu").click(function() {
        $(".item").toggleClass("item-activate");
    });


});