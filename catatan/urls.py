from django.urls import path
from . import views

urlpatterns = [
    path('', views.catatan, name = 'catatan'),
    path('post', views.post_catatan),
    path('data', views.dataCatatan),
    path('<str:nama_cat>', views.detail_catatan),
    path('delete/<int:id>', views.delete_catatan, name = 'delete_catatan'),
]