from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import modelCatatan
from .forms import catatanForm
from django.db.models import F
import requests as res
import json
from django.core import serializers

# Create your views here.

def post_catatan(request):
    if request.method == 'POST':
        response_data = {}
        form = catatanForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            modelCatatan.objects.create(nama = request.POST['nama'],
                                        fakultas = request.POST['fakultas'],
                                        matkul = request.POST['matkul'],
                                        deskripsi = request.POST['deskripsi'],
                                        link = request.POST['link'],)
            return redirect('/catatan/#catatanTerdaftar')
        else:
            return redirect('/catatan')

   
def catatan(request):
    form_tambah = catatanForm()
    data = modelCatatan.objects.all()
    response ={
        'form_tambah' : form_tambah,
        'data' : data,
    }
    return render(request, 'catatan.html', response)

def detail_catatan(request, nama_cat):
    data = modelCatatan.objects.filter(matkul = nama_cat)
    datas = modelCatatan.objects.all()
    response = {
        'data' : data,
        'datas' : datas,
    }
    return render(request, 'catatan-2.html', response)

def delete_catatan(request, id):
    data = modelCatatan.objects.filter(id = id)
    data.delete()
    return redirect('/catatan/detail')

def dataCatatan(request):
    data_catatanFix = modelCatatan.objects.all()
    catatan_json = serializers.serialize('json', data_catatanFix)
    isiData = json.loads(catatan_json)
    return JsonResponse(isiData, safe=False)
