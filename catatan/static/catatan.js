$(document).ready(function(){
    $('#bton').click(function(){
        var name = $("#nama").val();
        var link = $("#link").val();
        var matkul = $("#matkul").val();
        var fakultas = $("#fakultas").val();
        var desk = $("#desk").val();
        if((name.length != 0) && (link.length != 0) && (matkul.length != 0) && (fakultas.length != 0) && (desk.length != 0)){
            Swal.fire({
                    title: "Success",
                    text: "Your Note is Succesfully Added",
                    icon: "success",
                    });
            return true;
        }
        else{
            Swal.fire({
                    title: "Error",
                    text: "Your Notes Is Still Empty",
                    icon: "error",
                    });
            return true;
        }
    })

    $("#nama").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#nama").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#matkul").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#matkul").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#desk").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#desk").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#link").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#link").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })
    $("#fakultas").focus(function(){
        $(this).css("background-color", "#ffefbf");
    })
    $("#fakultas").blur(function(){
        $(this).css("background-color", "#FFFFFF");
    })

    $("form").on("submit", function(e) {
        var isiForm  = JSON.parse(JSON.stringify(jQuery(this).serializeArray()))
        $.ajax({
            type: "POST",
            url: "/catatan/post",
            data: isiForm,
            success: function(response){
                var result = '';
                for(var i = 0; i < response.length; i++){
                    var nama = response[i].fields.nama;
                    var matkul = response[i].fields.matkul;
                    result += '<tr>' +'<td style="width: 400px;">'+ '<p style="text-align: center;">'+ nama + '</p>'  + '<br>' + '</td>' + '<td style="width: 500px;">' + '<a style="color: #555555;" href="/catatan/{{ i.matkul }}">' + '<br>' + '<p style="text-align: center;">' + matkul + '</p> ' + '<br>'+ '</a>' + '</td>' + '</tr>'
                }
                $('#result-bar').append(result);
            }
        })
        e.preventDefault();
    })
})