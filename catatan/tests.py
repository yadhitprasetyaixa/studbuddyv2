from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import modelCatatan
from .forms import catatanForm
from . import views

# Create your tests here.

class UnitTestForCatatan(TestCase):
    def test_response_page(self):
        response = Client().get('/catatan/')
        self.assertEqual(response.status_code, 200)
    
    def test_response_page_two(self):
        response = Client().get('/catatan/<str:nama_cat>')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_for_begin(self):
        response = Client().get('/catatan/')
        self.assertTemplateUsed(response, 'catatan.html')

    def test_template_used_for_next(self):
        response = Client().get('/catatan/<str:nama_cat>')
        self.assertTemplateUsed(response, 'catatan-2.html')
    
    def test_isi_dalam_page_two(self):
        response = Client().get('/catatan/<str:nama_cat>')
        isi_page = response.content.decode('utf8')
        self.assertIn('User', isi_page)
        self.assertIn('Courses', isi_page)

    def test_func_page(self):
        found = resolve('/catatan/')
        self.assertEqual(found.func, views.catatan)

    def test_save_catatan_not_Valid(self):
        Client().post('/catatan/<str:nama_cat>', data={'nama_cat': 'hasbi'})
        jumlah = modelCatatan.objects.filter(nama='hasbi').count()
        self.assertEqual(jumlah, 0)
    
    def test_save_catatan_valid(self):
        obj = modelCatatan.objects.create(nama='hasbi')
        Client().post('/catatan/<str:nama_car>' + str(obj.id), data={'nama_cat' : 'hasbi'})
        jumlah = modelCatatan.objects.filter(nama='hasbi').count()
        self.assertEqual(jumlah, 1)

    def test_func_page_two(self):
        found = resolve('/catatan/<str:nama_cat>')
        self.assertEqual(found.func, views.detail_catatan)

    def test_delete_page_two(self):
        Client().post('/catatan/delete/<int:id>' , data={'id': 2})
        hapus = modelCatatan.objects.filter(id = 2).count()
        self.assertEqual(hapus, 0)
    
    def test_save_catatan_proper(self):
        obj = modelCatatan.objects.create(nama='hasbi', fakultas='pacil', matkul='sda', deskripsi='test', link='www.hasbi.com')
        Client().post('/catatan/post')
        cobs = modelCatatan.objects.count()
        self.assertEqual(cobs, 1)
    
    def test_form_in_views(self):
        form_data = {'nama':'hasbi', 'fakultas':'pacil','matkul':'sda','deskripsi':'kepo','link':'www.hasbi.com'}
        form = catatanForm(data=form_data)
        self.assertTrue(form.is_valid)
    
       
    

    
    
